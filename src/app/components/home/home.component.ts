import { Component, OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked,
  AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <h3>Ng style</h3>
    <app-ng-style></app-ng-style>
    <hr>
    <h3>Css scope</h3>
    <app-css></app-css>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad amet, est praesentium quisquam quo velit. Ad
      blanditiis consequuntur dolores facilis nulla obcaecati officiis. Assumenda dignissimos id odio repellendus
      sapiente.
    </p>
    <h3>Ng class</h3>
    <app-clases-css></app-clases-css>

    <hr>
    <h3>Directivas personalizadas</h3>
    <p [appResaltado]="'#f00'">Hola directiva personalizada</p>
    <hr>

    <hr>
    <h3>Ng switch</h3>
    <app-ng-switch></app-ng-switch>

  `,
  styles: []
})
export class HomeComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked,
  AfterViewInit, AfterViewChecked, OnDestroy {

  constructor() {
    console.log("constructor");

  }

  ngOnInit() {
    console.log("ngOnInit");
  }
  ngOnChanges(){
    console.log("ngOnChanges");
  }
  ngDoCheck(){
    console.log("ngDoCheck");
  }
  ngAfterContentInit(){
    console.log("ngAfterContentInit");
  }
  ngAfterContentChecked(){
    console.log("ngAfterContentChecked");
  }
  ngAfterViewInit(){
    console.log("ngAfterViewInit");
  }
  ngAfterViewChecked(){
    console.log("ngAfterViewChecked");
  }
  ngOnDestroy(){
    console.log("ngOnDestroy");
  }

}
