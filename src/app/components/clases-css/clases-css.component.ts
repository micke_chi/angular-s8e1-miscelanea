import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clases-css',
  templateUrl: './clases-css.component.html',
  styleUrls: ['./clases-css.component.css']
})
export class ClasesCssComponent implements OnInit {

  alerta: string = 'alert-danger';
  propiedades: object = {
    'danger' : false
  };

  cargador: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  ejecutarProceso(){
    this.cargador = true;
    setTimeout(()=> this.cargador = false, 3000)
  }
}
