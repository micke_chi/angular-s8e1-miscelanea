import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasesCssComponent } from './clases-css.component';

describe('ClasesCssComponent', () => {
  let component: ClasesCssComponent;
  let fixture: ComponentFixture<ClasesCssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasesCssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasesCssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
