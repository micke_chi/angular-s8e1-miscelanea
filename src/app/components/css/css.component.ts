import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-css',
  template: `
    <p>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus blanditiis culpa dignissimos eligendi 
      nesciunt non quo repellat tenetur ut, vitae. Amet animi dolorum fugiat neque optio quaerat quas qui voluptas!
    </p>
  `,
  styles: [
    `p{
      color: red;
      font-weight: bold;
    }`
  ]
})
export class CssComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
