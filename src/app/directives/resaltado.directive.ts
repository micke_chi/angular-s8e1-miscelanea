import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor(private el: ElementRef) {
    console.log("la directiva se llamo");
  }

  @Input('appResaltado')
  nuevoColor:string;

  @HostListener('mouseenter')
  mouseEntro(){
    console.log("el nuevo color", this.nuevoColor);
    this.resaltar(this.nuevoColor || 'yelow');
  }

  @HostListener('mouseleave')
  mouseSalio(){
    this.resaltar(null);
  }

  private resaltar(color: string){
    this.el.nativeElement.style.backgroundColor = color;
  }



}
